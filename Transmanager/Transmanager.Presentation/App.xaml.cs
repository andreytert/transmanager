﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Transmanager.Applications.ViewModels;
using System.Waf.Applications;
using System.Reflection;
using Transmanager.DataAccess;

namespace Transmanager.Presentation.Views
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private AggregateCatalog catalog;
        private CompositionContainer container;
        private MainWindowViewModel mainWindowViewModel;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(Controller).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(MainWindowViewModel).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(ITransmanagerContext).Assembly));

            container = new CompositionContainer(catalog);
            CompositionBatch batch = new CompositionBatch();
            batch.AddExportedValue(container);
            container.Compose(batch);

            mainWindowViewModel = container.GetExportedValue<MainWindowViewModel>();

            mainWindowViewModel.Initialize();

        }
    }
}
