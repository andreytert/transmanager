﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transmanager.DataAccess;
using Transmanager.Domain;
using Transmanager.Applications.utils;
using Transmanager.Applications.Views;

namespace Transmanager.Presentation.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [Export(typeof(IMainWindowView))]
    public partial class MainWindow : Window, IMainWindowView
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //this.b1.Command.Execute(null);
            MenuItemExit.Command.Execute(null);
        }

        private void MenuItemExit_Click_1(object sender, RoutedEventArgs e)
        {
            //this.Close();
        }


    }
}
