﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using Transmanager.Applications.Views;

namespace Transmanager.Presentation.Views
{
    /// <summary>
    /// Interaction logic for RequestListView.xaml
    /// </summary>
    
    [Export (typeof(IRequestListView))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class RequestListView : UserControl, IRequestListView
    {
        public RequestListView()
        {
            InitializeComponent();
        }

        public ICommand EditRequestCommand { get; set; }

        private void DataGrid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(EditRequestCommand != null)
                EditRequestCommand.Execute(null);
        }


    }
}
