﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transmanager.Applications.Views;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace Transmanager.Presentation.Views
{
    /// <summary>
    /// Interaction logic for CarView.xaml
    /// </summary>
    
    [Export(typeof(ICarView))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class CarView : UserControl,ICarView
    {
        public CarView()
        {
            InitializeComponent();
        }

    }
}
