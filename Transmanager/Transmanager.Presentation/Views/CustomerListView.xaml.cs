﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transmanager.Applications.Views;

namespace Transmanager.Presentation.Views
{
    /// <summary>
    /// Interaction logic for CustomerListView.xaml
    /// </summary>
    
    [Export(typeof(ICustomerListView))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class CustomerListView : UserControl, ICustomerListView
    {
        public CustomerListView()
        {
            InitializeComponent();
        }

        public ICommand EditCommand { get; set; }

        private void DataGrid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(EditCommand != null)
                EditCommand.Execute(null);
        }
    }
}
