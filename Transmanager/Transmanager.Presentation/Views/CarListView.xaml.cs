﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using Transmanager.Applications.Views;

namespace Transmanager.Presentation.Views
{
    /// <summary>
    /// Interaction logic for CarListView.xaml
    /// </summary>
    
    [Export(typeof(ICarListView))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class CarListView : UserControl,ICarListView
    {

        public CarListView()
        {
            InitializeComponent();
        }

        public ICommand EditCommand { get; set; }

        private void DataGrid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(EditCommand != null)
                EditCommand.Execute(null);
        }
    }
}
