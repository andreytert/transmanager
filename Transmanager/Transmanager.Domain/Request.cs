﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Domain
{

    public class Request
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public string ResponsiblePersonName { get; set; }
        public string ResponsiblePersonPhone { get; set; }
        public Contract Contract { get; set; }
        public Organization Customer { get; set; }
        public Organization Performer { get; set; }
        public Organization Shipper { get; set; }
        public Address AddressLoading { get; set; }
        public DateTime Loading { get; set; }
        public Address AddressUnloading { get; set; }
        public DateTime Unloading { get; set; }
        public string Cost { get; set; }
        public decimal CostDouble { get; set; }
        public Driver Driver { get; set; }
        public Car Car { get; set; }
                
    }
}
