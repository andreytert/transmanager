﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Domain
{
    public class Contract
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
    }
}
