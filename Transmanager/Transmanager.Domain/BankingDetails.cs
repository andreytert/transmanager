﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Domain
{
    public class BankingDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string RS { get; set; }
        public string KS { get; set; }
        public string Bik { get; set; }
        public string Inn { get; set; }
        public string Ogrn { get; set; }
    }
}
