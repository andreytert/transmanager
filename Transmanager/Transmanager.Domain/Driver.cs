﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Domain
{
    public class Driver
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Pasport Pasports { get; set; }
        public string PhoneNumber { get; set; }
        public string DriverLicense { get; set; }
        public Address AddresReal { get; set; }
        
        public Nullable<int> CarId { get; set; }

        public virtual Car Car { get; set; }
    }
}

