﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Domain
{
    public class Organization
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Address AddressPoch { get; set; }
        public Address AddressJurid { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Email { get; set; }
        public string Www { get; set; }
        public string Director { get; set; }
        public string ContactName { get; set; }
        public BankingDetails Bank { get; set; }
    }
}
