﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Domain
{
    public class Car
    {
        public int Id { get; set; }
        public string CarBrand { get; set; }
        public string Number { get; set; }
        public string TrailerNumber { get; set; }
        public String TechInspect { get; set; }
        //public Driver Owner { get; set; }
        public string Type { get; set; }
        public double Tonnage { get; set; }
        public double Capacity { get; set; }
        public double Volume { get; set; }
        
        public string Comment { get; set; }

        public virtual ICollection<Driver> Drivers { get; set; }

    }
}
