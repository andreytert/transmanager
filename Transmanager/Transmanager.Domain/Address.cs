﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Domain
{
    public class Address
    {
        public int Id { get; set; }
        public int Index { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Galaxy { get; set; }
    }
}
