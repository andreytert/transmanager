﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Domain
{
    public class Person
    {
        private static List<Person> responsiblePersons = new List<Person>();
        
        static Person()
        {
            responsiblePersons.Add(new Person() { Name = "Матвеев А.С.", Phone = "8-968-827-50-01" });
            responsiblePersons.Add(new Person() { Name = "Говорова Е.Г.", Phone = "8-905-507-26-37" });
            responsiblePersons.Add(new Person() { Name = "Есиков А.В.", Phone = "8-903-778-24-70" });
        }

        public string Name { get; set; }
        public string Phone { get; set; }

        public static IEnumerable<Person> GetResponsiblePersons()
        {
            return responsiblePersons;
        }

    }
}
