﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Domain
{
    public class Pasport
    {
        public int Id { get; set; }
        public string Series { get; set; }
        public string Number { get; set; }
        public Nullable<DateTime> IssuanceDate { get; set; }
        public string IssuingAuthority { get; set; }
        public Address AddresRegistration { get; set; }
    }
}
