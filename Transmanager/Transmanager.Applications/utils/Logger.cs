﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Transmanager.Applications.utils
{
    public class Logger
    {
        private static FileInfo _fileInfo;
        
        static Logger()
        {
            _fileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "logs.txt");
        }

        public static void AddMessage(string text)
        {
            StreamWriter sw = _fileInfo.AppendText();
            string tmp = DateTime.Now.ToString();
            sw.WriteLine(DateTime.Now.ToString() + " " + text);
            sw.Close();
        }
    }
}
