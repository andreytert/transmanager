﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using Novacode;
using Transmanager.Domain;

namespace Transmanager.Applications.utils
{
    public class WordReguestCreator
    {
        private string templatePath;

        public WordReguestCreator(string templatePath)
        {
            this.templatePath = templatePath;
        }

        private string GetMonthNameByNum(int num)
        {
            string thing;
            switch (num)
            {
                case 1:
                    thing = "Января";
                    break;
                case 2:
                    thing = "Февраля";
                    break;
                case 3:
                    thing = "Марта";
                    break;
                case 4:
                    thing = "Апреля";
                    break;
                case 5:
                    thing = "Мая";
                    break;
                case 6:
                    thing = "Июня";
                    break;
                case 7:
                    thing = "Июля";
                    break;
                case 8:
                    thing = "Августа";
                    break;
                case 9:
                    thing = "Сентября";
                    break;
                case 10:
                    thing = "Октября";
                    break;
                case 11:
                    thing = "Ноября";
                    break;
                case 12:
                    thing = "Декабря";
                    break;
                default:
                    throw new Exception("Month is wrong");
            }
            return thing;
        }

        // Transmanager.
        public bool Execute(Request req, string outputFileName)
        {

            string file_name;
            int num_fields = 40;
            string[] things = new string[num_fields];
            string message = "";
            if (req == null)
            {
                message += "Выберите заявку";
                return false;
            }


            if (req.AddressLoading == null) message += "Выберите адрес погрузки\n";
            if (req.AddressUnloading == null) message += "Выберите адрес разгрузки\n";
            if (req.Car == null) message += "Выберите Тип Т/С\n";
            if (req.Customer == null) message += "Выберите заказчика\n";
            if (req.Driver == null) message += "Выберите водителя\n";
            if (req.Performer == null) message += "Выберите исполнителя\n";
            if (req.Shipper == null) message += "Выберите грузоотправителя\n";

            if (message != "")
            {
                MessageBox.Show(message);
                return false;
            }

            DocX doc = DocX.Load(templatePath);
            try
            {
                //Номер доки
                doc.ReplaceText("e+x001", req.Number != null ? req.Number.ToString() : " ");
                //
                doc.ReplaceText("e+x002", req.Date.Day.ToString());
                //
                doc.ReplaceText("e+x003", GetMonthNameByNum(req.Date.Month));
                //
                doc.ReplaceText("e+x004", req.Date.Year.ToString());
                //Заказчик
                doc.ReplaceText("e+x005", req.Customer.Name ?? " ");
                //Исполнитель
                doc.ReplaceText("e+x006", req.Performer.Name ?? " ");
                //Место погрузки
                doc.ReplaceText("e+x007", (req.AddressLoading.City ?? " ") + ", " + (req.AddressLoading.Street ?? " "));
                //Грузоотправитель
                doc.ReplaceText("e+x008", req.Shipper.Name ?? " ");
                //День погрузки
                doc.ReplaceText("e+x009", req.Loading.Day.ToString());
                //Месяц погрузки, Год, час
                doc.ReplaceText("e+x010", GetMonthNameByNum(req.Loading.Month));
                doc.ReplaceText("e+x011", req.Loading.Year.ToString());
                //Требуемый тип транспортного средства
                doc.ReplaceText("e+x013", req.Car.Tonnage.ToString() + "т " + (req.Car.Type ?? " ") + " " + req.Car.Volume.ToString() + "М3");
                //things[12] = req.Car.Tonnage.ToString() + "т " + req.Car.Type+" "+ req.Car.Volume.ToString()+"М3";                //Адрес и дата разгрузки
                doc.ReplaceText("e+x014", (req.AddressUnloading.City ?? " ") + ", " + (req.AddressUnloading.Street ?? " ") + ", «" + req.Unloading.Day.ToString()
                    + "» " + GetMonthNameByNum(req.Unloading.Month) + "   " + req.Date.Year.ToString() + "г.");
                //Стоимость перевозки
                doc.ReplaceText("e+x015", req.Cost ?? " ");
                //Фио водителя
                doc.ReplaceText("e+x016", req.Driver.Name ?? " ");

                //Моб телефон вод
                doc.ReplaceText("e+x017", req.Driver.PhoneNumber ?? " ");
                //things[16] = req.Driver.PhoneNumber;
                //Паспорт
                if (req.Driver.Pasports != null)
                    doc.ReplaceText("e+x018", (req.Driver.Pasports.Series != null ? req.Driver.Pasports.Series.ToString() : " ") + " " + 
                        (req.Driver.Pasports.Number != null ? req.Driver.Pasports.Number.ToString() : " ") + " " +
                                 (req.Driver.Pasports.IssuingAuthority ?? " ") + ", " +
                                 req.Driver.Pasports.IssuanceDate.Value.Day.ToString() + " "
                                 + GetMonthNameByNum(req.Driver.Pasports.IssuanceDate.Value.Month) + " "
                                 + req.Driver.Pasports.IssuanceDate.Value.Year.ToString() + "г.");

                //Исполнитель Юр. адрес: Почтовый адрес:
                if (req.Performer.AddressJurid != null)
                    doc.ReplaceText("e+x019", req.Performer.AddressJurid.Index + ", " + (req.Performer.AddressJurid.City ?? " ") +
                             ", " + (req.Performer.AddressJurid.Street ?? " " ));
                if (req.Performer.AddressPoch != null)
                    doc.ReplaceText("e+x034", req.Performer.AddressPoch.Index + ", " + (req.Performer.AddressPoch.City ?? " ") 
                        + ", " + (req.Performer.AddressPoch.Street ?? " "));

                if (req.Performer.Bank != null)
                {
                    doc.ReplaceText("e+x020", (req.Performer.Bank.RS ?? " ") + ", " + (req.Performer.Bank.Name ?? " "));
                    //к/с 
                    doc.ReplaceText("e+x021", req.Performer.Bank.KS ?? " ");
                    //inn
                    doc.ReplaceText("e+x022", req.Performer.Bank.Inn ?? " ");
                    //БИК
                    doc.ReplaceText("e+x023", req.Performer.Bank.Bik ?? " ");
                }
                //cite
                doc.ReplaceText("e+x024", req.Performer.Www ?? " ");

                if (req.Customer.AddressJurid != null)
                    doc.ReplaceText("e+x025", req.Customer.AddressJurid.Index + ", " + (req.Customer.AddressJurid.City ?? " ") 
                        + ", " + (req.Customer.AddressJurid.Street ?? " "));
                //"Почтовый адрес: "
                if (req.Customer.AddressPoch != null)
                    doc.ReplaceText("e+x026", req.Customer.AddressPoch.Index + ", " + (req.Customer.AddressPoch.City ?? " ") 
                        + ", " + (req.Customer.AddressPoch.Street ?? " "));
                if (req.Customer.Bank != null)
                {
                    //rs
                    doc.ReplaceText("e+x027", (req.Customer.Bank.RS ?? " ") + ", " + (req.Customer.Bank.Name ?? " "));
                    //ks
                    doc.ReplaceText("e+x028", req.Customer.Bank.KS ?? " ");
                    //inn
                    doc.ReplaceText("e+x029", req.Customer.Bank.Inn ?? " ");
                    doc.ReplaceText("e+x030", req.Customer.Bank.Ogrn ?? " ");
                    //bik
                    doc.ReplaceText("e+x031", req.Customer.Bank.Bik ?? " ");
                }
                //cite
                doc.ReplaceText("e+x032", req.Customer.Www ?? " ");
                //things[31] = req.Customer.Www;

                if (req.Performer.Bank != null)
                {
                    //ответственное лицо заказчика
                    doc.ReplaceText("e+x033", req.Performer.Bank.Ogrn ?? " ");
                    //ответственное лицо исполнителя
                    doc.ReplaceText("e+x034", req.Performer.Bank.Bik ?? " ");
                }

                doc.ReplaceText("e+x035", req.Car.CarBrand ?? " ");
                doc.ReplaceText("e+x036", req.Car.Number ?? " ");
                doc.ReplaceText("e+x037", req.Car.Type ?? " ");
                doc.ReplaceText("e+x038", req.Car.Tonnage.ToString() + "т");
                doc.ReplaceText("e+x039", req.Car.Capacity.ToString());
            }
            catch (Exception ex)
            {
                Logger.AddMessage(ex.Message);
                MessageBox.Show("Что-то пошло не так");
                return false;
            }

            doc.ReplaceText("e+resp_name", req.ResponsiblePersonName ?? " ");
            doc.ReplaceText("e+resp_phone", req.ResponsiblePersonPhone ?? " ");


            //Шаблон для Debug лежит в корне проекта, для релиза шаблон будет лежать вместе с экзешником

            doc.SaveAs(outputFileName);
            MessageBox.Show("Договор успешно создан");
            return true;
        }


    }
}