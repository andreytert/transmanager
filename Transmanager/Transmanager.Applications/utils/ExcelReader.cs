﻿using Excel = Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Transmanager.Applications.utils
{
    public class ExcelReader
    {
        private static Excel.Application appExcel;
        private static Excel.Workbook newWorkbook = null;
        private static Excel._Worksheet objsheet = null;
        //private static Logger _logger = new Logger();

        private static Excel.Range _range;

        public static Excel.Range Range
        {
            get { return _range; }
            private set { _range = value; }
        }
        
        //Method to initialize opening Excel
        public static void excel_init(String path)
        {
            appExcel = new Excel.Application();
            if (System.IO.File.Exists(path))
            {
                // then go and load this into excel
                newWorkbook = appExcel.Workbooks.Open(path, true, true);
                objsheet = (Excel._Worksheet)appExcel.ActiveWorkbook.ActiveSheet;
                Range = objsheet.UsedRange;
            }
            else
            {
                Logger.AddMessage("Unable to open file!");
                System.Runtime.InteropServices.Marshal.ReleaseComObject(appExcel);
                appExcel = null;
                System.Windows.Forms.Application.Exit();
            }

        }

        //Method to get value; cellname is A1,A2, or B1,B2 etc...in excel.
        public static string excel_getValue(string cellname)
        {
            string value = string.Empty;
            try
            {
                value = objsheet.get_Range(cellname).get_Value().ToString();
            }
            catch
            {
                value = "";
            }

            return value;
        }

        //Method to close excel connection
        public static void excel_close()
        {
            if (appExcel != null)
            {
                try
                {
                    newWorkbook.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(appExcel);
                    appExcel = null;
                    objsheet = null;
                }
                catch (Exception ex)
                {
                    appExcel = null;
                    Logger.AddMessage("Unable to release the Object " + ex.ToString());
                }
                finally
                {
                    GC.Collect();
                }
            }
        }
    }
}
