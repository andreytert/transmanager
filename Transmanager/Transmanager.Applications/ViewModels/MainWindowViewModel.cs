﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Threading;
using Microsoft.Data.ConnectionUI;
using Transmanager.Applications.Views;
using Transmanager.Applications.utils;
using Transmanager.Domain;
using Transmanager.DataAccess;
using System.Waf.Presentation.Services;
using System.Collections.Specialized;

namespace Transmanager.Applications.ViewModels
{

    [Export]
    public class MainWindowViewModel : ViewModel<IMainWindowView>
    {
        private ObservableCollection<IPage> pages;
        private CompositionContainer container;
        private DelegateCommand showDriversCommand;
        private DelegateCommand showCustomerCommand;
        private DelegateCommand connectionDialogCommand;
        private DelegateCommand showRequestCommand;
        private DelegateCommand showCarCommand;
        private DelegateCommand closeMainWindowCommand;
        private DelegateCommand _loadDriversCommand;

        private MessageService messageService;

        private ContextFactory _contextFactory;

        protected bool enabledPanelButtons = false;
   
        protected Thread  thread_db_connect;
        protected Timer thread_watch_timer;

        private int _selectedIndex;
        
        [ImportingConstructor]
        public MainWindowViewModel(IMainWindowView view, CompositionContainer container, ContextFactory contextFactory)
            : base(view)
        {
            this.container = container;
            this.pages = new ObservableCollection<IPage>();
            this.messageService = new MessageService();
            showDriversCommand = new DelegateCommand(ShowDrivers);
            showCustomerCommand = new DelegateCommand(ShowCustomer);
            showRequestCommand = new DelegateCommand(ShowRequest);
            showCarCommand = new DelegateCommand(ShowCar);
            ConnectionDialogCommand = new DelegateCommand(ShowConnectionDialog);
            closeMainWindowCommand = new DelegateCommand(CloseMainWindow);
            _loadDriversCommand = new DelegateCommand(LoadDrivers);

            _contextFactory = contextFactory;

            //Многопоточная загрузка базы
            thread_db_connect = new Thread(new ParameterizedThreadStart(db_connect_proc));
            thread_db_connect.Start(contextFactory);

           thread_watch_timer = new Timer(thread_watch_proc, null, 500, 500);
        }

        public void LoadDrivers()
        {
            //STRONGLY DEPENDS ON THE FORMAT FILE
            
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Title = "Выберите файл с водителями";
            dlg.Filter = dlg.Filter = "Excel documents|*.xlsx;*.xls | All files|*.*";
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                string filename = dlg.FileName;
                ExcelReader.excel_init(filename);
                for (int r = 2; r < ExcelReader.Range.Rows.Count; r++)
                {
                    Driver driver = new Driver();
                    string name = ExcelReader.excel_getValue("F" + String.Format("{0}", r));
                    string phoneNumber = ExcelReader.excel_getValue("H" + String.Format("{0}", r));
                    driver.Name = name;
                    driver.PhoneNumber = phoneNumber;

                    Address addrpoch = new Address();
                    Pasport pasport = new Pasport();
                    Address addrurid = new Address();
                    driver.AddresReal = addrpoch;
                    driver.Pasports = pasport;
                    driver.Pasports.AddresRegistration = addrurid;
                    driver.Pasports.IssuanceDate = DateTime.Now;

                    ITransmanagerContext context = _contextFactory.CreateContext();
                    context.Drivers.Add(driver);
                    context.SaveChanges();
                }
                ExcelReader.excel_close();
            }
        }

        public void AddPage(IPage page)
        {
            int ind = -1;
            for(int i = 0; i < pages.Count; i ++)
                if (pages[i].Title == page.Title)
                    ind = i;
            if (ind != -1)
                pages.RemoveAt(ind);
            pages.Add(page);
            SelectedIndex = pages.Count - 1;
        }

        public void RemovePage(IPage page)
        {
            pages.Remove(page);
        }

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                if (!value.Equals(_selectedIndex))
                {
                    _selectedIndex = value;
                    RaisePropertyChanged("SelectedIndex");
                }
            }
        }

        protected void db_connect_proc(object contextFactObj)
        {
            try
            {
                ContextFactory contextFactory = (ContextFactory)contextFactObj;
                ITransmanagerContext context = contextFactory.CreateContext();
                ICollection<Driver> records = context.Drivers.Where(v => v.Id == 0).ToList();
            }
            catch (Exception ex)
            {
                //messageService.ShowError(View, "Невозможно соединиться с базой причина:\n" + ex.ToString());
            }
        }
        protected void thread_watch_proc(object obj)
        {
            if ( thread_db_connect.IsAlive == false)
            {
                enabledPanelButtons = true;
                RaisePropertyChanged("EnabledPanelButtons");
                thread_watch_timer.Dispose();
                thread_watch_timer = null;
                thread_db_connect = null;
            }
        }

        private void CloseMainWindow()
        {
            //word_handle.release();
        }

        private void ShowConnectionDialog()
        {
            DataConnectionDialog dcd = new DataConnectionDialog();
            dcd.DataSources.Add(DataSource.SqlDataSource);
            dcd.UnspecifiedDataSource.Providers.Add(DataProvider.SqlDataProvider);

            dcd.ConnectionString = DataAccess.Settings.Default.ConnectionString;

            
            if (DataConnectionDialog.Show(dcd) == System.Windows.Forms.DialogResult.OK)
            {
                DataAccess.Settings.Default.ConnectionString = dcd.ConnectionString;
                DataAccess.Settings.Default.Save();
            }
        }

        private void ShowCar()
        {
            AddPage(container.GetExportedValue<CarListViewModel>());
        }
        private void ShowDrivers()
        {
            AddPage(container.GetExportedValue<DriverListViewModel>());
        }
        private void ShowRequest()
        { 
            AddPage(container.GetExportedValue<RequestListViewModel>());
        }
        private void ShowCustomer()
        {
            AddPage(container.GetExportedValue<CustomerListViewModel>());
        }

        public void Initialize()
        {
            ViewCore.Show();
        }

        public IEnumerable<IPage> Pages
        {
            get { return pages; }
        }

        public DelegateCommand ShowCarCommand
        {
            get { return showCarCommand; }
            set
            {
                if (value.Equals(showCarCommand))
                    return;
                showCarCommand = value;
                RaisePropertyChanged("ShowCarCommand");
            }
        }
        public DelegateCommand ShowDriversCommand
        {
            get { return showDriversCommand; }
            private set
            {
                if (value.Equals(showDriversCommand))
                    return;
                showDriversCommand = value;
                RaisePropertyChanged("ShowDriversCommand");
            }
        }
      
        public DelegateCommand ShowRequestCommand
        {
            get { return showRequestCommand; }
            set
            {
                if (value.Equals(showRequestCommand))
                    return;
                showRequestCommand = value;
                RaisePropertyChanged("ShowRequestCommand");
            }
        }
        public DelegateCommand ShowCustomerCommand
        {
            get { return showCustomerCommand; }
            private set
            {
                if (value.Equals(showCustomerCommand))
                    return;
                showCustomerCommand = value;
                RaisePropertyChanged("ShowCustomerCommand");
            }
        }        
        public DelegateCommand ConnectionDialogCommand
        {
            get { return connectionDialogCommand; }
            set
            {
                if(value.Equals(connectionDialogCommand))
                    return;
                connectionDialogCommand = value;
                RaisePropertyChanged("ConnectionDialogCommand");
            }
        }
        
        public DelegateCommand CloseMainWindowCommand
        {
            get { return closeMainWindowCommand; }
            set
            {
                if (value.Equals(closeMainWindowCommand))
                    return;
                closeMainWindowCommand = value;
                RaisePropertyChanged("CloseMainWindowCommand");
            }
        }
        
        public bool EnabledPanelButtons
        {
            get { return enabledPanelButtons; }
            set
            {
                enabledPanelButtons = value;
                RaisePropertyChanged("EnabledPanelButtons");
            }

        }

        public DelegateCommand LoadDriversCommand 
        {
            get { return _loadDriversCommand; }
            set
            {
                if (!value.Equals(_loadDriversCommand))
                {
                    _loadDriversCommand = value;
                    RaisePropertyChanged("LoadDriversCommand");
                }
            }
        }
    }
}
