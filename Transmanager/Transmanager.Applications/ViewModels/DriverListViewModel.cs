﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using System.Windows.Input;
using Transmanager.Applications.Views;
using Transmanager.DataAccess;
using Transmanager.Domain;

namespace Transmanager.Applications.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DriverListViewModel : ViewModel<IDriverListView>, IPage
    {
        private DelegateCommand closeCommand;
        private MainWindowViewModel mainWindowViewModel;
        private ContextFactory contextFactory;
        private ICollection<Driver> records;
        private DelegateCommand newDriverCommand;
        private CompositionContainer container;
        private DelegateCommand refreshCommand;
        private bool isSelectionMode;
        private DelegateCommand selectCommand;
        private DelegateCommand removeCommand;
        private DelegateCommand editDriverCommand;
        private Driver selectedValue;
        private string title;

        [ImportingConstructor]
        public DriverListViewModel(IDriverListView view, MainWindowViewModel mainWindowViewModel,
                                   ContextFactory contextFactory, CompositionContainer container)
            : base(view)
        {
            this.mainWindowViewModel = mainWindowViewModel;
            this.contextFactory = contextFactory;
            this.container = container;
            Title = "Водители";
            closeCommand = new DelegateCommand(ClosePage);
            newDriverCommand = new DelegateCommand(AddNewDriver);
            refreshCommand = new DelegateCommand(Refresh);
            selectCommand = new DelegateCommand(ClosePage);
            removeCommand = new DelegateCommand(Remove);
            editDriverCommand = new DelegateCommand(EditDriver);
            view.EditDriverCommand = editDriverCommand;
            Refresh();
        }

        private void EditDriver()
        {
            DriverViewModel viewModel = container.GetExportedValue<DriverViewModel>();
            viewModel.Driver = selectedValue;
            mainWindowViewModel.AddPage(viewModel);
            AddWeakEventListener(viewModel, DriverViewEventHandler);
        }

        private void Remove()
        {
            ITransmanagerContext context = contextFactory.CreateContext();
            context.SaveChanges();
            context.Drivers.Load();
            foreach (Driver v in context.Drivers)
                if (v.Id == selectedValue.Id)
                    context.Drivers.Remove(v);
            context.SaveChanges();
            Refresh();
        }
        private void Refresh()
        {
            ITransmanagerContext context = contextFactory.CreateContext();
            context.SaveChanges();
            records = context.Drivers.Where(v => true)
                .Include("AddresReal")
                .Include("Pasports")
                .Include("Pasports.AddresRegistration")
                .ToList();
            RaisePropertyChanged("Records");
        }
        private void DriverViewEventHandler(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsExecuteAdd")
                Refresh();
        }

        private void AddNewDriver()
        {
            DriverViewModel viewModel = container.GetExportedValue<DriverViewModel>();
            mainWindowViewModel.AddPage(viewModel);
            AddWeakEventListener(viewModel, DriverViewEventHandler);
        }
        private void ClosePage()
        {
            mainWindowViewModel.RemovePage(this);
        }

        public Driver SelectedValue
        {
            get { return selectedValue; }
            set { selectedValue = value; RaisePropertyChanged("SelectedValue"); }
        }
        public string Title
        {
            get { return title; }
            private set
            {
                if (title != value)
                {
                    title = value;
                    RaisePropertyChanged("Title");
                }
            }
        }
        public ICollection<Driver> Records
        {
            get { return records; }
        }

        public DelegateCommand SelectCommand
        {
            get { return selectCommand; }
        }
        public DelegateCommand RemoveCommand
        {
            get { return removeCommand; }
        }
        public ICommand CloseCommand
        {
            get { return closeCommand; }
        }
        public DelegateCommand NewDriverCommand
        {
            get { return newDriverCommand; }
            set { newDriverCommand = value; RaisePropertyChanged("NewDriverCommand"); }
        }
        public DelegateCommand RefreshCommand
        {
            get { return refreshCommand; }
        }

        public bool IsSelectionMode
        {
            get { return isSelectionMode; }
            set { isSelectionMode = value; RaisePropertyChanged("IsSelectionMode"); }
        }
    }
}
