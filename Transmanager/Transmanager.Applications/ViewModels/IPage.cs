﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Transmanager.Applications.ViewModels
{
    public interface IPage
    {
        string Title { get; }
        object View { get; }
        ICommand CloseCommand { get; }
    }
}
