﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using System.Windows.Input;
using Transmanager.Applications.Views;
using Transmanager.DataAccess;
using Transmanager.Domain;

namespace Transmanager.Applications.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CustomerViewModel:ViewModel<ICustomerView>, IPage
    {
        private string title;
        private MainWindowViewModel mainWindowViewModel;
        private DelegateCommand closeCommand;
        private DelegateCommand saveCommand;
        private DelegateCommand equivalentCommand;

        private ContextFactory contextFactory;
        private Organization customer;
        private bool isExecuteAdd;
        private bool equivalent;

        [ImportingConstructor]
        public CustomerViewModel(ICustomerView view, MainWindowViewModel mainWindowViewModel, ContextFactory contextFactory)
            : base(view)
        {
            this.contextFactory = contextFactory;
            this.mainWindowViewModel = mainWindowViewModel;
            title = "Добавление контрагента";
            closeCommand = new DelegateCommand(Close);
            saveCommand = new DelegateCommand(Save);
            equivalentCommand = new DelegateCommand(Equivil);
            BankingDetails cust = new BankingDetails();
            Address uridaddr = new Address();
            Address pochaddr = new Address();
            customer = new Organization();
            customer.Bank = cust;
            customer.AddressJurid = uridaddr;
            customer.AddressPoch = pochaddr;
            isExecuteAdd = false;
            equivalent = false;
        }

        private void Equivil()
        {
            if (equivalent)
            {
                customer.AddressPoch.City = customer.AddressJurid.City;
                customer.AddressPoch.Index = customer.AddressJurid.Index;
                customer.AddressPoch.Street = customer.AddressJurid.Street;
            }
            else
            {
                customer.AddressPoch.City = null;
                customer.AddressPoch.Index = 0;
                customer.AddressPoch.Street = null;
            }
            RaisePropertyChanged("Customer");
        }

        private void Save()
        {

            ITransmanagerContext context = contextFactory.CreateContext();
            
            if(!isEditingMode)
                context.Organizations.Add(customer);         
            else
            {
                context.Organizations.Attach(customer);
                context.Addresses.Attach(customer.AddressJurid);
                context.Addresses.Attach(customer.AddressPoch);
                context.BankingDetailses.Attach(customer.Bank);

                context.UpdateEntity(customer);
                context.UpdateEntity(customer.AddressJurid);
                context.UpdateEntity(customer.AddressPoch);
                context.UpdateEntity(customer.Bank);

            }

            context.SaveChanges();
            Close();
            IsExecuteAdd = true;
        }

        private void Close()
        {
            mainWindowViewModel.RemovePage(this);
        }

        private Organization LoadCustomer(int id)
        {
            var context = contextFactory.CreateContext();
            return context.Organizations.Where(v => v.Id == id)
                          .Include(v => v.AddressJurid)
                          .Include(v => v.AddressPoch)
                          .Include(v => v.Bank)
                          .First();
        }

        public Organization Customer
        {
            get { return customer; }
            set
            { 
                customer = LoadCustomer(value.Id);
                RaisePropertyChanged("Customer"); 
            }
        }

        public string Title
        {
            get { return title; }
            private set
            {
                if(title.Equals(value))
                    return;
                title = value;
                RaisePropertyChanged("Title");
            }
        }

        public DelegateCommand EquivalentCommand
        {
            get { return equivalentCommand; }            
        }

        public bool IsExecuteAdd
        {
            get { return isExecuteAdd; }
            set
            {
                isExecuteAdd = value;
                RaisePropertyChanged("IsExecuteAdd");
            }
        }

        public ICommand CloseCommand
        {
            get { return closeCommand; }
        }

        public ICommand SaveCommand
        {
            get { return saveCommand; }
        }

        public bool Equivalent
        {
            get { return equivalent; }
            set 
            {
                if (equivalent != value)
                    equivalent = value;
                RaisePropertyChanged("Equivalent");
            }
        }

        private bool isEditingMode;
        public bool IsEditingMode
        {
            get { return isEditingMode; }
            set
            {
                if(isEditingMode == value)
                    return;
                isEditingMode = value;
                if(isEditingMode)
                    Title = "Редактирование контрагента";
                else
                    Title = "Добавление контрагента";
                RaisePropertyChanged("IsEditingMode");
            }

        }

    }
}
