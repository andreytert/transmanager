﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using System.Windows.Forms;
using System.Windows.Input;
using Transmanager.Applications.Views;
using Transmanager.DataAccess;
using Transmanager.Domain;

namespace Transmanager.Applications.ViewModels
{
    public delegate void SavedHandler(object sender, EventArgs e);

    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class RequestViewModel : ViewModel<IRequestView>, IPage
    {
        public event SaveHandler Saved;

        private CompositionContainer container;
        private DelegateCommand closeCommand;
        private MainWindowViewModel mainWindowViewModel;
        private ContextFactory contextFactory;

        private DelegateCommand selectDriverCommand;
        private DelegateCommand selectCustomerCommand;
        private DelegateCommand saveCommand;
        private DelegateCommand _equivalentCustomerCommand;
        private DelegateCommand _equivalentShipperCommand;
        private DelegateCommand selectPerformerCommand;
        private DelegateCommand selectShipperCommand;
        private DelegateCommand selectCarCommand;

        private Driver driver;
        private Organization customer;
        private Request request;

        private bool _equivalentCustomer;
        private bool _equivalentShipper;

        private IEnumerable<string> _cityList;

        [ImportingConstructor]
        public RequestViewModel(IRequestView view, CompositionContainer container, 
            MainWindowViewModel mainWindowViewModel, ContextFactory contextFactory)
            : base(view)
        {
            title = "Добавление Заявки";
            this.mainWindowViewModel = mainWindowViewModel;
            this.container = container;
            this.contextFactory = contextFactory;
            closeCommand = new DelegateCommand(Close);
            selectCustomerCommand = new DelegateCommand(SelectCustomer);
            selectDriverCommand = new DelegateCommand(SelectDriver);
            saveCommand = new DelegateCommand(Save);
            _equivalentCustomerCommand = new DelegateCommand(SetLikeCustomer);
            _equivalentShipperCommand = new DelegateCommand(SetLikeShipper);
            selectPerformerCommand = new DelegateCommand(SelectPerformer);
            selectShipperCommand = new DelegateCommand(SelectShipper);
            selectCarCommand = new DelegateCommand(SelectCar);
            request = new Request();
            request.AddressLoading = new Address();
            request.AddressUnloading = new Address();
            request.Date = request.Loading = request.Unloading = DateTime.Now;

            ITransmanagerContext context = contextFactory.CreateContext();
            _cityList = context.Addresses.Select(v => v.City).Distinct().ToList();
        }

        private void SelectCar()
        {
            CarListViewModel model = container.GetExportedValue<CarListViewModel>();
            model.IsSelectionMode = true;
            mainWindowViewModel.AddPage(model);
            AddWeakEventListener(model, CarListListner);
        }
        private void CarListListner(object sender, PropertyChangedEventArgs e)
        {
            CarListViewModel model = sender as CarListViewModel;
            if (e.PropertyName == "SelectedValue")
            {
                request.Car = model.SelectedValue;
                RaisePropertyChanged("Request");
            }
        }
        private void RaiseSaved()
        {
            if (Saved != null)
                Saved(this, null);
        }
        public bool EquivalentCustomer
        {
            get { return _equivalentCustomer; }
            set
            {
                if (!value.Equals(_equivalentCustomer))
                {
                    _equivalentCustomer = value;
                    if (value)
                        EquivalentShipper = !value;
                    RaisePropertyChanged("EquivalentCustomer");
                    RaisePropertyChanged("HitTestVisible");
                }
            }
        }
        public bool EquivalentShipper
        {
            get { return _equivalentShipper; }
            set
            {
                if (!value.Equals(_equivalentShipper))
                {
                    _equivalentShipper = value;
                    if (value)
                        EquivalentCustomer = !value;
                    RaisePropertyChanged("EquivalentShipper");
                    RaisePropertyChanged("HitTestVisible");
                }
            }
        }
        public bool HitTestVisible
        {
            get { return !EquivalentCustomer; }
        }
        

        public IEnumerable<string> CityList
        {
            get { return _cityList; }
        }

        private void SetLikeCustomer()
        {
            if (EquivalentCustomer && Request.Customer != null)
            {
                Request.AddressLoading.City = Request.Customer.AddressPoch.City;
                Request.AddressLoading.Street = Request.Customer.AddressPoch.Street;
            }
            else
                Request.AddressLoading.City = Request.AddressLoading.Street = null;
            RaisePropertyChanged("Request");
        }
        private void SetLikeShipper()
        {
            if (EquivalentShipper && Request.Shipper != null)
            {
                Request.AddressLoading.City = Request.Shipper.AddressPoch.City;
                Request.AddressLoading.Street = Request.Shipper.AddressPoch.Street;
            }
            else
                Request.AddressLoading.City = Request.AddressLoading.Street = null;
            RaisePropertyChanged("Request");
        }
        private void SelectPerformer()
        {
            CustomerListViewModel viewModel = container.GetExportedValue<CustomerListViewModel>();
            AddWeakEventListener(viewModel, PerformerListEventHandler);
            viewModel.IsSelectionMode = true;
            mainWindowViewModel.AddPage(viewModel);
        }
        private void PerformerListEventHandler(object sender, PropertyChangedEventArgs e)
        {
            CustomerListViewModel customerList = (CustomerListViewModel)sender;
            if (e.PropertyName == "SelectedValue")
            {
                request.Performer = customerList.SelectedValue;
                EquivalentCustomer = false;
                EquivalentShipper = false;
                RaisePropertyChanged("Request");
            }
        }
        private void SelectShipper()
        {
            CustomerListViewModel viewModel = container.GetExportedValue<CustomerListViewModel>();
            AddWeakEventListener(viewModel, ShipperListEventHandler);
            viewModel.IsSelectionMode = true;
            mainWindowViewModel.AddPage(viewModel);
        }
        private void ShipperListEventHandler(object sender, PropertyChangedEventArgs e)
        {
            CustomerListViewModel customerList = (CustomerListViewModel)sender;
            if (e.PropertyName == "SelectedValue")
            {
                request.Shipper = customerList.SelectedValue;
                EquivalentCustomer = false;
                EquivalentShipper = false;
                RaisePropertyChanged("Request");
            }
        }
        private void SelectDriver()
        {
            DriverListViewModel viewModel = container.GetExportedValue<DriverListViewModel>();
            AddWeakEventListener(viewModel, DriverListEventHandler);
            viewModel.IsSelectionMode = true;
            mainWindowViewModel.AddPage(viewModel);
        }
        private void SelectCustomer()
        {
            CustomerListViewModel viewModel = container.GetExportedValue<CustomerListViewModel>();
            AddWeakEventListener(viewModel, CustomerListEventHandler);
            viewModel.IsSelectionMode = true;
            mainWindowViewModel.AddPage(viewModel);
        }
        private void CustomerListEventHandler(object sender, PropertyChangedEventArgs e)
        {
            CustomerListViewModel customerList = (CustomerListViewModel) sender;
            if (e.PropertyName == "SelectedValue")
            {
                request.Customer = customerList.SelectedValue;
                EquivalentCustomer = false;
                EquivalentShipper = false;
                RaisePropertyChanged("Request");
            }
        }
        private void DriverListEventHandler(object sender, PropertyChangedEventArgs e)
        {
            DriverListViewModel driverList = (DriverListViewModel) sender;
            if (e.PropertyName == "SelectedValue")
            {
                request.Driver = driverList.SelectedValue;
                RaisePropertyChanged("Request");
            }
        }
        private void Close()
        {
            mainWindowViewModel.RemovePage(this);
        }

        private string title;
        public string Title
        {
            get { return title; }
            private set
            {
                if(value.Equals(title))
                    return;
                title = value;
                RaisePropertyChanged("Title");
            }
        }

        public ICommand CloseCommand
        {
            get { return closeCommand; }
        }
        public ICommand SaveCommand
        {
            get { return saveCommand; }
        }
        public DelegateCommand SelectDriverCommand
        {
            get { return selectDriverCommand; }
        }
        public DelegateCommand SelectCustomerCommand
        {
            get { return selectCustomerCommand; }
        }
        public DelegateCommand EquivalentCustomerCommand
        {
            get { return _equivalentCustomerCommand; }
        }
        public DelegateCommand EquivalentShipperCommand
        {
            get { return _equivalentShipperCommand; }
        }
        public DelegateCommand SelectCarCommand
        {
            get { return selectCarCommand; }
        }
        public DelegateCommand SelectPerformerCommand
        {
            get { return selectPerformerCommand; }
        }
        public DelegateCommand SelectShipperCommand
        {
            get { return selectShipperCommand; }
        }

        public Request Request
        {
            get { return request; }
            set { request = value; }
        }

        public Driver Driver
        {
            get { return driver; }
            set { driver = value; RaisePropertyChanged("Driver"); }
        }

        public Organization Customer
        {
            get { return customer; }
            set { customer = value; RaisePropertyChanged("Customer"); }
        }

        

        

        private void Save()
        {
            ITransmanagerContext context = contextFactory.CreateContext();

            string message = "";

            if (request.AddressLoading == null) message += "Выберите адрес погрузки\n";
            if (request.AddressUnloading == null) message += "Выберите адрес разгрузки\n";
            if (request.Car == null) message += "Выберите Тип Т/С\n";
            if (request.Customer == null) message += "Выберите заказчика\n";
            if (request.Driver == null) message += "Выберите водителя\n";
            if (request.Performer == null) message += "Выберите исполнителя\n";
            if (request.Shipper == null) message += "Выберите грузоотправителя\n";

            if (message != "")
            {
                MessageBox.Show(message, "Внимание!");
                return;
            }

            if (isEditingMode)
            {
                context.Requests.Attach(request);
                context.UpdateEntity(request);
                if (request.AddressLoading != null)
                {
                    context.Addresses.Attach(request.AddressLoading);
                    context.UpdateEntity(request.AddressLoading);
                }

                if (request.AddressUnloading != null)
                {
                    context.Addresses.Attach(request.AddressUnloading);
                    context.UpdateEntity(request.AddressUnloading);
                }
            }
          
            if(request.Driver != null)
            {
                context.Drivers.Attach(request.Driver);
                context.UpdateEntity(request.Driver);
            }

            if (request.Car != null)
            {
                context.Cars.Attach(request.Car);
                context.UpdateEntity(request.Car);
            }

            if (request.Shipper != null)
            {
                context.Organizations.Attach(request.Shipper);
                
                context.UpdateEntity(request.Shipper);
            }

            if (request.Customer != null && request.Customer.Id != request.Shipper.Id)
            {
                context.Organizations.Attach(request.Customer);
                context.UpdateEntity(request.Customer);
            }
            if(request.Performer != null && request.Customer.Id != request.Performer.Id && request.Performer.Id != request.Shipper.Id)
            {
                context.Organizations.Attach(request.Performer);
                context.UpdateEntity(request.Performer);
            }

            if (!isEditingMode)
                context.Requests.Add(request);

            context.SaveChanges();
            Close();
        }

        private bool isEditingMode;
        public bool IsEditingMode
        {
            get { return isEditingMode; }
            set
            {
                if (isEditingMode == value)
                    return;

                isEditingMode = value;
                if (isEditingMode)
                    Title = "Редактирование заявки";
                else
                    Title = "Создание заявки";
            }
        }

        public IEnumerable<Person> ResponsiblePersons
        {
            get { return Person.GetResponsiblePersons(); }
        }

        public Person ResponsiblePerson
        {
            get
            {
                if (request.ResponsiblePersonName == null)
                    return null;
                return Person.GetResponsiblePersons().Where(v => v.Name == request.ResponsiblePersonName).FirstOrDefault();
            }
            set 
            {
                request.ResponsiblePersonName = value.Name;
                request.ResponsiblePersonPhone = value.Phone;
                RaisePropertyChanged("ResponsiblePerson");
            }
        }

    }
}
