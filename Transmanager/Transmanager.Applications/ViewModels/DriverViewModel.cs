﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using System.Windows.Input;
using Transmanager.Applications.Views;
using Transmanager.DataAccess;
using Transmanager.Domain;

namespace Transmanager.Applications.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DriverViewModel : ViewModel<IDriverView>, IPage
    {
        private string title;
        private MainWindowViewModel mainWindowViewModel;
        private DelegateCommand closeCommand;
        private DelegateCommand saveCommand;
        private DelegateCommand equivalentCommand;

        private ContextFactory contextFactory;
        private Driver driver;
        private bool equivalent;
        private bool isExecuteAdd;

        [ImportingConstructor]
        public DriverViewModel(IDriverView view, MainWindowViewModel mainWindowViewModel, ContextFactory contextFactory)
            : base(view)
        {
            this.contextFactory = contextFactory;
            this.mainWindowViewModel = mainWindowViewModel;
            title = "Добавление водителя";
            closeCommand = new DelegateCommand(Close);
            saveCommand = new DelegateCommand(Save);
            equivalentCommand = new DelegateCommand(Equivil);
            driver = new Driver();
            Address addrpoch = new Address();
            Pasport pasport = new Pasport();
            Address addrurid = new Address();
            driver.AddresReal = addrpoch;
            driver.Pasports = pasport;
            driver.Pasports.AddresRegistration = addrurid;
            driver.Pasports.IssuanceDate = DateTime.Now;
            equivalent = false;
            isExecuteAdd = false;
        }

        private void Equivil()
        {
            if (equivalent)
            {
                driver.AddresReal.City = driver.Pasports.AddresRegistration.City;
                driver.AddresReal.Index = driver.Pasports.AddresRegistration.Index;
                driver.AddresReal.Street = driver.Pasports.AddresRegistration.Street;
            }
            else
            {
                Driver.AddresReal.City = null;
                Driver.AddresReal.Index = 0;
                Driver.AddresReal.Street = null;
            }
            RaisePropertyChanged("Driver");
        }       

        private void Save()
        {
            ITransmanagerContext context = contextFactory.CreateContext();
            if (driver.Id == 0)
            {
                context.Drivers.Add(driver);
                context.SaveChanges();
            }
            else
            {
                if (driver.Pasports != null)
                {
                    context.Pasports.Attach(driver.Pasports);
                    context.UpdateEntity(driver.Pasports);
                    if (driver.Pasports.AddresRegistration != null)
                    {
                        context.Addresses.Attach(driver.Pasports.AddresRegistration);
                        context.UpdateEntity(driver.Pasports.AddresRegistration);
                    }
                }
                if (driver.AddresReal != null)
                {
                    context.Addresses.Attach(driver.AddresReal);
                    context.UpdateEntity(driver.AddresReal);
                }
                context.Drivers.Attach(Driver);
                context.UpdateEntity(Driver);
                context.SaveChanges();
            }
            Close();
            IsExecuteAdd = true;
        }
        private void Close()
        {
            mainWindowViewModel.RemovePage(this);
        }

        public Driver Driver
        {
            get { return driver; }
            set
            {
                if (value == null || value.Id == 0)
                    throw new ArgumentException("Driver has'n id");

                var context = contextFactory.CreateContext();

                driver = context.Drivers
                                .Include(v => v.AddresReal)
                                .Include(v => v.Pasports.AddresRegistration).First(v => v.Id == value.Id);

                RaisePropertyChanged("Driver");
            }
        }

        public string Title
        {
            get { return title; }
        }

        public ICommand CloseCommand
        {
            get { return closeCommand; }
        }
        public ICommand SaveCommand
        {
            get { return saveCommand; }
        }
        public DelegateCommand EquivalentCommand
        {
            get { return equivalentCommand; }
        }

        public bool Equivalent
        {
            get { return equivalent; }
            set
            {
                equivalent = value;
                RaisePropertyChanged("Equivalent");                
            } 
        }
        public bool IsExecuteAdd
        {
            get { return isExecuteAdd; }
            set 
            {
                isExecuteAdd = value;
                RaisePropertyChanged("IsExecuteAdd");
            }
        }
    }
}
