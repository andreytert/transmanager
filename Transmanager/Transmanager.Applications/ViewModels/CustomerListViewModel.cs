﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using System.Windows.Input;
using Transmanager.Applications.Views;
using Transmanager.DataAccess;
using Transmanager.Domain;

namespace Transmanager.Applications.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CustomerListViewModel:ViewModel<ICustomerListView>, IPage
    {
        private DelegateCommand closeCommand;
        private MainWindowViewModel mainWindowViewModel;
        private ContextFactory contextFactory;
        private ICollection<Organization> records;
        private DelegateCommand newCustomerCommand;
        private CompositionContainer container;
        private DelegateCommand refreshCommand;
        private DelegateCommand removeCommand;
        private bool isSelectionMode;
        private DelegateCommand selectCommand;
        private Organization selectedValue;
        private string title;

        [ImportingConstructor]
        public CustomerListViewModel(ICustomerListView view, MainWindowViewModel mainWindowViewModel,
                                   ContextFactory contextFactory, CompositionContainer container)
            : base(view)
        {
            this.mainWindowViewModel = mainWindowViewModel;
            this.contextFactory = contextFactory;
            this.container = container;
            Title = "Контрагенты";
            closeCommand = new DelegateCommand(ClosePage);
            newCustomerCommand = new DelegateCommand(AddNewCustomer);
            refreshCommand = new DelegateCommand(Refresh);
            selectCommand = new DelegateCommand(ClosePage);
            removeCommand = new DelegateCommand(Remove);
            
            this.ViewCore.EditCommand  = new DelegateCommand(EditCustomer);

            Refresh();
        }

        private void EditCustomer()
        {
            CustomerViewModel model = container.GetExportedValue<CustomerViewModel>();
            model.IsEditingMode = true;
            model.Customer = selectedValue;
            mainWindowViewModel.AddPage(model);
        }

        private void Remove()
        {
            ITransmanagerContext context = contextFactory.CreateContext();
            context.SaveChanges();
            context.Organizations.Load();
            foreach (Organization v in context.Organizations)
                if (v.Id == selectedValue.Id)
                    context.Organizations.Remove(v);
            context.SaveChanges();
            Refresh();
        }

        private void CustomerViewEventHandler(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsExecuteAdd")
                Refresh();
        }

        private void Refresh()
        {
            ITransmanagerContext context = contextFactory.CreateContext();
            records = context.Organizations.Where(v => true)
                .Include("AddressJurid")
                .Include("AddressPoch")
                .Include("Bank")
                .ToList();
            RaisePropertyChanged("Records");
        }
        private void AddNewCustomer()
        {
            CustomerViewModel viewModel = container.GetExportedValue<CustomerViewModel>();
            mainWindowViewModel.AddPage(viewModel);
            AddWeakEventListener(viewModel, CustomerViewEventHandler);
        }
        private void ClosePage()
        {
            mainWindowViewModel.RemovePage(this);
        }

        public DelegateCommand SelectCommand
        {
            get { return selectCommand; }
            set { selectCommand = value; RaisePropertyChanged("SelectCommand"); }
        }
        public ICommand CloseCommand
        {
            get { return closeCommand; }
        }
        public DelegateCommand NewCustomerCommand
        {
            get { return newCustomerCommand; }
            set { newCustomerCommand = value; RaisePropertyChanged("NewCustomerCommand"); }
        }
        public DelegateCommand RefreshCommand
        {
            get { return refreshCommand; }
        }
        public DelegateCommand RemoveCommand
        {
            get { return removeCommand; }
        }

        public Organization SelectedValue
        {
            get { return selectedValue; }
            set
            {
                selectedValue = value; 
                RaisePropertyChanged("SelectedValue");
            }
        }
        public string Title
        {
            get { return title; }
            private set
            {
                if (title != value)
                {
                    title = value;
                    RaisePropertyChanged("Title");
                }
            }
        }
        public ICollection<Organization> Records
        {
            get { return records; }
        }
        public bool IsSelectionMode
        {
            get { return isSelectionMode; }
            set { isSelectionMode = value; RaisePropertyChanged("IsSelectionMode"); }
        }
    }
}
