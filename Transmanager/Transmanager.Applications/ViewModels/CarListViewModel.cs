﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using System.Data.Entity;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using Transmanager.Applications.Views;
using Transmanager.DataAccess;
using Transmanager.Domain;
using System.Windows.Input;

namespace Transmanager.Applications.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CarListViewModel:ViewModel<ICarListView>, IPage
    {
        private string title;
        private CompositionContainer container;
        private ContextFactory contextFactory;
        private MainWindowViewModel mainWindowViewModel;
        private ICollection<Car> records;
        private Car selectedValue;

        private DelegateCommand newCarCommand;
        private DelegateCommand closeCommand;
        private DelegateCommand refreshCommand;
        private DelegateCommand removeCommand;
        private DelegateCommand selectCommand;

        private bool isSelectionMode;

        [ImportingConstructor]
        public CarListViewModel(CompositionContainer container, ContextFactory contextFactory, MainWindowViewModel mainWindowViewModel, ICarListView view)
            : base(view)
        {
            this.title = "Машины";
            this.container = container;
            this.contextFactory = contextFactory;
            this.mainWindowViewModel = mainWindowViewModel;

            newCarCommand = new DelegateCommand(CarNewPage);
            closeCommand=new DelegateCommand(ClosePage);
            refreshCommand = new DelegateCommand(Refresh);
            removeCommand = new DelegateCommand(Remove);
            selectCommand = new DelegateCommand(Select);

            ViewCore.EditCommand = new DelegateCommand(EditCar);

            Refresh();
        }

        private void EditCar()
        {
            CarViewModel model = container.GetExportedValue<CarViewModel>();
            model.Saved += (sender, args) => Refresh();
            model.IsEditingMode = true;
            model.Car = selectedValue;
            mainWindowViewModel.AddPage(model);
        }

        private void Select()
        {
            mainWindowViewModel.RemovePage(this);
        }

        private void Remove()
        {
            ITransmanagerContext context = contextFactory.CreateContext();
            context.SaveChanges();
            context.Cars.Load();
            context.Drivers.Load();
            foreach (Car v in context.Cars)
                if (v.Id == selectedValue.Id)
                    context.Cars.Remove(v);
            context.SaveChanges();
            Refresh();
        }

        private void CarNewPage()
        {
            CarViewModel viewModel = container.GetExportedValue<CarViewModel>();
            mainWindowViewModel.AddPage(viewModel);
        }
        public DelegateCommand NewCarCommand
        {
            get { return newCarCommand; }
            set 
            {
                newCarCommand = value;
                RaisePropertyChanged("CarNewPageCommand");
            }
        }

        public DelegateCommand RemoveCommand
        {
            get { return removeCommand; }
        }

        private void Refresh()
        {
            ITransmanagerContext context = contextFactory.CreateContext();
            records = context.Cars.Where(v => true)
                .Include("Drivers")
                .ToList();
            RaisePropertyChanged("Records");
        }
        public DelegateCommand RefreshCommand
        {
            get { return refreshCommand; }
        }

        private void ClosePage()
        {
            mainWindowViewModel.RemovePage(this);
        }
        public ICommand CloseCommand
        {
            get { return closeCommand; }
        }

        public string Title
        {
            get { return title; }
        }
        public ICollection<Car> Records
        {
            get { return records; }
            set
            {
                records = value;
                RaisePropertyChanged("Records");
            }
        }
        public Car SelectedValue
        {
            get { return selectedValue; }
            set { selectedValue = value; RaisePropertyChanged("SelectedValue"); }
        }

        public bool IsSelectionMode
        {
            get { return isSelectionMode; }
            set { isSelectionMode = value; RaisePropertyChanged("IsSelectionMode"); }
        }

        public DelegateCommand SelectCommand
        {
            get { return selectCommand; }
        }
    }
}
