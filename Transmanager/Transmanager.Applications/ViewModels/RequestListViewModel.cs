﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using Transmanager.Applications.Views;
using Transmanager.DataAccess;
using Transmanager.Domain;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data.Entity;
using System.Windows.Input;
using Transmanager.Applications.utils;

namespace Transmanager.Applications.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RequestListViewModel:ViewModel<IRequestListView>,IPage
    {
        private MainWindowViewModel mainWindowViewModel;
        private CompositionContainer container;
        private ContextFactory contextFactory;
        private ICollection<Request> records;
        private string title;

        private DelegateCommand addRequestCommand;
        private DelegateCommand closePageCommand;
        private DelegateCommand refreshCommand;
        private DelegateCommand createContractCommand;

        private DelegateCommand editRequestCommand;

        [ImportingConstructor]
        public RequestListViewModel(MainWindowViewModel mainWindowViewModel, IRequestListView view,
            CompositionContainer container, ContextFactory context)
            : base(view)
        {
            this.container = container;
            this.contextFactory = context;
            this.mainWindowViewModel = mainWindowViewModel;
            this.title = "Заявки";

            AddRequestCommand = new DelegateCommand(RequestNewPage);
            closePageCommand = new DelegateCommand(() => CloseRequestPage());
            refreshCommand = new DelegateCommand(Refresh);
            createContractCommand = new DelegateCommand(CreateContract);
            editRequestCommand = new DelegateCommand(Edit);
            ViewCore.EditRequestCommand = editRequestCommand;
            Refresh();
        }

        private void Edit()
        {
            RequestViewModel model = container.GetExportedValue<RequestViewModel>();
            model.IsEditingMode = true;
            model.Request = selectedValue;
            mainWindowViewModel.AddPage(model);
        }

        private void RequestNewPage()
        {
            RequestViewModel viewModel = container.GetExportedValue<RequestViewModel>();
            mainWindowViewModel.AddPage(viewModel);
        }
        public DelegateCommand AddRequestCommand
        {
            get { return addRequestCommand; }
            set
            {
                addRequestCommand = value;
                RaisePropertyChanged("AddRequestCommand");
            }
        }

        private void CreateContract()
        {
            if (selectedValue == null)
                return;
            WordReguestCreator word = new WordReguestCreator(AppDomain.CurrentDomain.BaseDirectory + "template.docx");
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Заявки\\"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Заявки\\");

            string fileName = AppDomain.CurrentDomain.BaseDirectory + "Заявки\\" + selectedValue.Customer.Name + " " +
                              selectedValue.Date.Day + '.' + selectedValue.Date.Month + '.' + selectedValue.Date.Year +
                              ".docx";
            word.Execute(selectedValue, fileName);
        }

        private void CloseRequestPage()
        {
            mainWindowViewModel.RemovePage(this);
        }
        public ICommand CloseCommand
        {
            get { return closePageCommand; }
        }

        private void Refresh()
        {
            ITransmanagerContext context = contextFactory.CreateContext();
            records = context.Requests.Where(v => true)
                .Include("Contract")
                .Include("Customer")
                .Include("Customer.AddressPoch")
                .Include("Customer.AddressJurid")
                .Include("Customer.Bank")
                .Include("Shipper")
                .Include("Shipper.AddressPoch")
                .Include("Shipper.AddressJurid")
                .Include("Shipper.Bank")
                .Include("Performer")
                .Include("Performer.AddressPoch")
                .Include("Performer.AddressJurid")
                .Include("Performer.Bank")
                .Include("AddressLoading")
                .Include("AddressUnloading")
                .Include("Car")
                .Include("Driver")
                .Include("Driver.AddresReal")
                .Include("Driver.Pasports")
                .Include("Driver.Pasports.AddresRegistration")
                .ToList();
            RaisePropertyChanged("Records");
        }
        public DelegateCommand RefreshCommand
        {
            get { return refreshCommand; }
        }

        private Request selectedValue;
        public Request SelectedValue
        {
            get { return selectedValue; }
            set
            {
                selectedValue = value;
                RaisePropertyChanged("SelectedValue");
            }
        }

        public ICollection<Request>  Records
        {
            get { return records; }
            set
            {
                records = value;
                RaisePropertyChanged("Records");
            }

        }
        public string Title
        {
            get { return title; }
            set
            {
                if (value != title)
                {
                    title = value;
                    RaisePropertyChanged("Title");
                }
            }
        }

        public ICommand CreateContractCommand
        {
            get { return createContractCommand; }
        }


    }
}
