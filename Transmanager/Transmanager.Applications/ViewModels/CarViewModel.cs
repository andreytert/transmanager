﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using System.Windows.Input;
using Transmanager.Applications.Views;
using Transmanager.DataAccess;
using Transmanager.Domain;
using System.Data.Entity;
namespace Transmanager.Applications.ViewModels
{
    public delegate void SaveHandler(object sender, EventArgs e);

    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CarViewModel:ViewModel<ICarView>,IPage
    {
        public event SaveHandler Saved;

        private string title;
        private ContextFactory contextFactory;
        private MainWindowViewModel mainWindowViewModel;
        private Car car;

        private DelegateCommand closeCommand;
        private DelegateCommand saveCommand;

        private List<string> carTypes;

        [ImportingConstructor]
        public CarViewModel(MainWindowViewModel mainWindowViewModel, ContextFactory contextFactory,  ICarView view)
            : base(view)
        {
            this.title = "Добавить машину";
            this.mainWindowViewModel = mainWindowViewModel;
            this.contextFactory = contextFactory;
            this.car = new Car();
        //    Driver driver = new Driver();
           // car.Owner = driver;

            closeCommand = new DelegateCommand(ClosePage);
            saveCommand = new DelegateCommand(SaveChanges);

            carTypes = new List<string>()
                {
                    "реф",
                    "изотерма",
                    "тент"
                };
        }

        private void RaiseSaved()
        {
            if (Saved != null)
                Saved(this, null);
        }

        private void ClosePage()
        {
            mainWindowViewModel.RemovePage(this);
        }

        public ICommand CloseCommand
        {
            get { return closeCommand; }
        }

        private void SaveChanges()
        {
            ITransmanagerContext context = contextFactory.CreateContext();

            if(!isEditingMode)
                context.Cars.Add(car);
            else
            {
                context.Cars.Attach(car);
                context.UpdateEntity(car);
            }
            context.SaveChanges();
            RaiseSaved();
            ClosePage();
        }
        public ICommand SaveCommand
        {
            get { return saveCommand; }
        }

        private Car LoadCar(int id)
        {
            var context = contextFactory.CreateContext();
            return context.Cars.Where(v => v.Id == id).Include(v => v.Drivers).First();
        }

        public Car Car
        {
            get { return car; }
            set
            {
                car = LoadCar(value.Id);
                RaisePropertyChanged("Car");
            }
        }
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                RaisePropertyChanged("Title");
            }
        }


        public List<string> CarTypes
        {
            get { return carTypes; }
        }

        private bool isEditingMode;
        public bool IsEditingMode
        {
            get { return isEditingMode; }
            set
            {
                if(isEditingMode == value)
                    return;

                isEditingMode = value;
                if (isEditingMode)
                    Title = "Редактирование машины";
                else
                    Title = "Добавить машину";
                RaisePropertyChanged("IsEditingMode");
            }
        }
    }
}
