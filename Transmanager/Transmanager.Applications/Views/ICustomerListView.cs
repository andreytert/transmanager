﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Waf.Applications;
using System.Windows.Input;

namespace Transmanager.Applications.Views
{
    public interface ICustomerListView:IView
    {
        ICommand EditCommand { get; set; }
    }
}
