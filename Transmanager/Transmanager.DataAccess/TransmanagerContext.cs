﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Transmanager.DataAccess.Migrations;
using Transmanager.Domain;

namespace Transmanager.DataAccess
{
    internal class TransmanagerContext: DbContext, ITransmanagerContext
    {
        static TransmanagerContext()
        {
           Database.SetInitializer(new MigrateDatabaseToLatestVersion<TransmanagerContext, Configuration>());
        }

        internal TransmanagerContext(string connectionString)
            : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Requesr
            modelBuilder.Entity<Request>().Property(v => v.Date).HasColumnType("datetime2");
            modelBuilder.Entity<Request>().Property(v => v.Loading).HasColumnType("datetime2");
            modelBuilder.Entity<Request>().Property(v => v.Unloading).HasColumnType("datetime2");
            #endregion
            #region Passport
            modelBuilder.Entity<Pasport>().Property(v => v.IssuanceDate).HasColumnType("datetime2");
            #endregion
        } 

        public void UpdateEntity(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public IDbSet<Address> Addresses { get; set; }
        public IDbSet<Driver> Drivers { get; set; }
        public IDbSet<Contract> Contracts { get; set; }
        public IDbSet<Car> Cars { get; set; }        
        public IDbSet<Organization> Organizations { get; set; }
        public IDbSet<BankingDetails> BankingDetailses { get; set; }
        public IDbSet<Request> Requests { get; set; }
        public IDbSet<Pasport> Pasports { get; set; }

    }
}
