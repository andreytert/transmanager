﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Transmanager.Domain;

namespace Transmanager.DataAccess
{
    public interface ITransmanagerContext
    {
        IDbSet<Driver> Drivers { get; }
        IDbSet<Request> Requests { get; }
        IDbSet<Contract> Contracts { get; }
        IDbSet<Car> Cars { get; }
        IDbSet<Address> Addresses { get; }
        IDbSet<Organization> Organizations { get; }
        IDbSet<BankingDetails> BankingDetailses { get; }
        IDbSet<Pasport> Pasports { get; }
        int SaveChanges();
        void UpdateEntity(object entity);
    }
}
