namespace Transmanager.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddResponsiblePerson : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requests", "ResponsiblePersonName", c => c.String());
            AddColumn("dbo.Requests", "ResponsiblePersonPhone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Requests", "ResponsiblePersonPhone");
            DropColumn("dbo.Requests", "ResponsiblePersonName");
        }
    }
}
