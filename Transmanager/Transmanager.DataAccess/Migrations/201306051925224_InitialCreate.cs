namespace Transmanager.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Index = c.Int(nullable: false),
                        City = c.String(),
                        Street = c.String(),
                        Galaxy = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhoneNumber = c.String(),
                        DriverLicense = c.String(),
                        CarId = c.Int(),
                        Pasports_Id = c.Int(),
                        AddresReal_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pasports", t => t.Pasports_Id)
                .ForeignKey("dbo.Addresses", t => t.AddresReal_Id)
                .ForeignKey("dbo.Cars", t => t.CarId)
                .Index(t => t.Pasports_Id)
                .Index(t => t.AddresReal_Id)
                .Index(t => t.CarId);
            
            CreateTable(
                "dbo.Pasports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Series = c.String(),
                        Number = c.String(),
                        IssuanceDate = c.DateTime(storeType: "datetime2"),
                        IssuingAuthority = c.String(),
                        AddresRegistration_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddresRegistration_Id)
                .Index(t => t.AddresRegistration_Id);
            
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CarBrand = c.String(),
                        Number = c.String(),
                        TrailerNumber = c.String(),
                        TechInspect = c.String(),
                        Type = c.String(),
                        Tonnage = c.Double(nullable: false),
                        Capacity = c.Double(nullable: false),
                        Volume = c.Double(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Number = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Telephone1 = c.String(),
                        Telephone2 = c.String(),
                        Email = c.String(),
                        Www = c.String(),
                        Director = c.String(),
                        ContactName = c.String(),
                        AddressPoch_Id = c.Int(),
                        AddressJurid_Id = c.Int(),
                        Bank_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddressPoch_Id)
                .ForeignKey("dbo.Addresses", t => t.AddressJurid_Id)
                .ForeignKey("dbo.BankingDetails", t => t.Bank_Id)
                .Index(t => t.AddressPoch_Id)
                .Index(t => t.AddressJurid_Id)
                .Index(t => t.Bank_Id);
            
            CreateTable(
                "dbo.BankingDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        RS = c.String(),
                        KS = c.String(),
                        Bik = c.String(),
                        Inn = c.String(),
                        Ogrn = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(),
                        Date = c.DateTime(nullable: false, storeType: "datetime2"),
                        Loading = c.DateTime(nullable: false, storeType: "datetime2"),
                        Unloading = c.DateTime(nullable: false, storeType: "datetime2"),
                        Cost = c.String(),
                        CostDouble = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Contract_Id = c.Int(),
                        Customer_Id = c.Int(),
                        Performer_Id = c.Int(),
                        Shipper_Id = c.Int(),
                        AddressLoading_Id = c.Int(),
                        AddressUnloading_Id = c.Int(),
                        Driver_Id = c.Int(),
                        Car_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contracts", t => t.Contract_Id)
                .ForeignKey("dbo.Organizations", t => t.Customer_Id)
                .ForeignKey("dbo.Organizations", t => t.Performer_Id)
                .ForeignKey("dbo.Organizations", t => t.Shipper_Id)
                .ForeignKey("dbo.Addresses", t => t.AddressLoading_Id)
                .ForeignKey("dbo.Addresses", t => t.AddressUnloading_Id)
                .ForeignKey("dbo.Drivers", t => t.Driver_Id)
                .ForeignKey("dbo.Cars", t => t.Car_Id)
                .Index(t => t.Contract_Id)
                .Index(t => t.Customer_Id)
                .Index(t => t.Performer_Id)
                .Index(t => t.Shipper_Id)
                .Index(t => t.AddressLoading_Id)
                .Index(t => t.AddressUnloading_Id)
                .Index(t => t.Driver_Id)
                .Index(t => t.Car_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Requests", "Car_Id", "dbo.Cars");
            DropForeignKey("dbo.Requests", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Requests", "AddressUnloading_Id", "dbo.Addresses");
            DropForeignKey("dbo.Requests", "AddressLoading_Id", "dbo.Addresses");
            DropForeignKey("dbo.Requests", "Shipper_Id", "dbo.Organizations");
            DropForeignKey("dbo.Requests", "Performer_Id", "dbo.Organizations");
            DropForeignKey("dbo.Requests", "Customer_Id", "dbo.Organizations");
            DropForeignKey("dbo.Requests", "Contract_Id", "dbo.Contracts");
            DropForeignKey("dbo.Organizations", "Bank_Id", "dbo.BankingDetails");
            DropForeignKey("dbo.Organizations", "AddressJurid_Id", "dbo.Addresses");
            DropForeignKey("dbo.Organizations", "AddressPoch_Id", "dbo.Addresses");
            DropForeignKey("dbo.Drivers", "CarId", "dbo.Cars");
            DropForeignKey("dbo.Drivers", "AddresReal_Id", "dbo.Addresses");
            DropForeignKey("dbo.Drivers", "Pasports_Id", "dbo.Pasports");
            DropForeignKey("dbo.Pasports", "AddresRegistration_Id", "dbo.Addresses");
            DropIndex("dbo.Requests", new[] { "Car_Id" });
            DropIndex("dbo.Requests", new[] { "Driver_Id" });
            DropIndex("dbo.Requests", new[] { "AddressUnloading_Id" });
            DropIndex("dbo.Requests", new[] { "AddressLoading_Id" });
            DropIndex("dbo.Requests", new[] { "Shipper_Id" });
            DropIndex("dbo.Requests", new[] { "Performer_Id" });
            DropIndex("dbo.Requests", new[] { "Customer_Id" });
            DropIndex("dbo.Requests", new[] { "Contract_Id" });
            DropIndex("dbo.Organizations", new[] { "Bank_Id" });
            DropIndex("dbo.Organizations", new[] { "AddressJurid_Id" });
            DropIndex("dbo.Organizations", new[] { "AddressPoch_Id" });
            DropIndex("dbo.Drivers", new[] { "CarId" });
            DropIndex("dbo.Drivers", new[] { "AddresReal_Id" });
            DropIndex("dbo.Drivers", new[] { "Pasports_Id" });
            DropIndex("dbo.Pasports", new[] { "AddresRegistration_Id" });
            DropTable("dbo.Requests");
            DropTable("dbo.BankingDetails");
            DropTable("dbo.Organizations");
            DropTable("dbo.Contracts");
            DropTable("dbo.Cars");
            DropTable("dbo.Pasports");
            DropTable("dbo.Drivers");
            DropTable("dbo.Addresses");
        }
    }
}
