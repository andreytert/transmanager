﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Data.Entity.Infrastructure;

namespace Transmanager.DataAccess
{

    [Export]
    public class ContextFactory
    {
        private InternalFactory iternalFactory;

        public ContextFactory()
        {
            this.iternalFactory = new InternalFactory();
        }

        public ITransmanagerContext CreateContext()
        {
            return iternalFactory.Create();
        }
    }

    internal class InternalFactory : IDbContextFactory<TransmanagerContext>
    {
        public TransmanagerContext Create()
        {
            return new TransmanagerContext(Settings.Default.ConnectionString);
        }
    }
}
